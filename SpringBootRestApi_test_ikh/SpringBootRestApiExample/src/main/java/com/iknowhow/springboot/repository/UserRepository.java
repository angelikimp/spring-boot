package com.iknowhow.springboot.repository;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.iknowhow.springboot.model.User;



@Repository
public class UserRepository{

	    @Autowired
	    JdbcTemplate jdbcTemplate;
	    
	public User findById(long id) {
	    return jdbcTemplate.queryForObject("select * from user where id=?", new Object[] {
	            id
	        },
	        new BeanPropertyRowMapper < User > (User.class));
	}
	
	public List < User > findAll() {
	    return jdbcTemplate.query("select * from user", new UserRowMapper());
	}
	
	class UserRowMapper implements RowMapper < User > {
	    @Override
	    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	        User user = new User();
	        user.setId(rs.getLong("id"));
	        user.setName(rs.getString("name"));
	        user.setAge(rs.getInt("age"));
	        user.setSalary(rs.getDouble("salary"));
	        return user;
	    }
	}
	
}
