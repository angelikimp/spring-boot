package com.iknowhow.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.iknowhow.springboot.model.User;
import com.iknowhow.springboot.service.UserService;
import com.iknowhow.springboot.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	UserService userService; //Service which will do all data retrieval/manipulation work

	// -------------------Retrieve All Users---------------------------------------------

	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public ResponseEntity<List<User>> listAllUsers() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        logger.info("User: -> {}", userService.findAllUsers());
		return ResponseEntity.accepted().headers(headers).body(userService.findAllUsers());
	}

	// -------------------Retrieve Single User------------------------------------------

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable("id") long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        logger.info("User {} : -> {}", id,userService.findById(id));
		return ResponseEntity.accepted().headers(headers).body(userService.findById(id));
	}

	// -------------------Create a User-------------------------------------------

	@RequestMapping(value = "/user/", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        userService.saveUser(user);
		return ResponseEntity.status(HttpStatus.CREATED).build(); //.headers(headers).build();
	}

	// ------------------- Update a User ------------------------------------------------

	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody User user) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        userService.updateUser(user);
		return ResponseEntity.status(HttpStatus.CREATED).headers(headers).build();
	}

	// ------------------- Delete a User-----------------------------------------

	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        userService.deleteUserById(id);
		return ResponseEntity.status(HttpStatus.CREATED).headers(headers).build();
	}

	// ------------------- Delete All Users-----------------------------

	@RequestMapping(value = "/user/", method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteAllUsers() {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Responded", "RestApiController");	
        userService.deleteAllUsers();
		return ResponseEntity.status(HttpStatus.CREATED).headers(headers).build();
	}

}